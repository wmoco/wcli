package shortener

import ( 
	"strings"
	"strconv"
)

func ShortenWithEllipsis(s string) string { 
	return shortenstr(s, 3) 
}

func ShortenWithSub(s string) string { 
	return shortenstr(s, 1) 
}

    //mode = 1->subscript, 3->ellipsis
func shortenstr(s string, mode int) string { 
    //  super="⁰¹²³⁴⁵⁶⁷⁸⁹"
    //    sub="₀₁₂₃₄₅₆₇₈₉"
    // supermp:=map[string]string{ "0":"⁰", "1":"¹", "2":"²", "3":"³", "4":"⁴", "5":"⁵", "6":"⁶", "7":"⁷", "8":"⁸", "9":"⁹"}

    submp:=map[string]string{ "0":"₀", "1":"₁", "2":"₂", "3":"₃", "4":"₄", "5":"₅", "6":"₆", "7":"₇", "8":"₈", "9":"₉" }
    scissors:= "✂" 
    midline_ellipsis:="⋯"

    l:=len(s)
    if l<9 {
        return s
    }
    var result strings.Builder

    numshown:=4
    if l>96 {
        numshown=32
    }

    if mode==1 { 
        // show length as a subscript string
        result.WriteString(s[:numshown]) 
        result.WriteString(midline_ellipsis)
        result.WriteString(midline_ellipsis)
        lstr:=strconv.Itoa(l)
        for _,c :=range(lstr) { 
            v,ok:=submp[string(c)]
            if ok { 
                result.WriteString(v)
            } else { 
                result.WriteString(string(c)) 
            }
        }
        result.WriteString(midline_ellipsis)
        result.WriteString(midline_ellipsis)
        result.WriteString(s[len(s)-numshown:])
    } else if mode==2 { 
        result.WriteString(s[:numshown]) 
        result.WriteString( scissors) 
        result.WriteString(s[len(s)-numshown:])

    } else if mode==3 { 
        result.WriteString(s[:numshown]) 
        result.WriteString( midline_ellipsis)
        result.WriteString(s[len(s)-numshown:])

    }
    return result.String()
}

