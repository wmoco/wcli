package main 

import ( 
	"fmt"
	"flag"
	"bufio"
	"os"
	"io"
	"gitlab.com/wmoco/wcli/nabwalya" 
	"gitlab.com/wmoco/wcli/shortener" 
)


func main() { 

	var flag_sub bool
	flag.BoolVar(&flag_sub, "sub", false, "write the length (of the whole, replaced string) in subscript ") 


	fmt.Printf("func returned: %s\n", nabwalya.Gizmo("thingybob") ) 
	//fmt.Println(".")

    rdr := bufio.NewReaderSize(os.Stdin,64 * 1024)
	var converted string
    for {
        line, _, err := rdr.ReadLine()
		converted=string(line) 
        if err == nil {
			if flag_sub { 
				converted=shortener.ShortenWithSub(converted)
			} else { 
				converted=shortener.ShortenWithEllipsis(converted)
			}
        } else if err == io.EOF {
            break
        } else {
            panic("oops")
        }
		fmt.Println(converted)
    }
}

